//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract StakingRewards is Ownable {
    using SafeERC20 for IERC20;

    struct EpochInfo {
        uint256 totalStakedAmount;
        uint256 totalUnstakedAmount;
    }

    struct EpochData {
        // total
        uint256 totalStake;
        uint256 rewardSize;
        uint256 rewardResidue;
        // user
        uint256 userStake;
        uint256 userReward;
        // all time
        uint256 allTimeReward;
        uint256 allTimeStake;
    }

    /* ========== STATE VARIABLES ========== */

    IERC20 public stakingToken;
    uint256 public EPOCH_DURATION = 7 days;
    uint256 public DEFAULT_REWARD_SIZE;
    uint256 public NUMBER_OF_EPOCHS = 8;
    uint256 public MAX_RATE_DENUM = 10;
    uint256 public MAX_RATE_NUM = 1;
    uint256 public BEGIN_DATE;

    mapping(address => uint256[]) public stakeholderToStakes;
    mapping(address => uint256[]) public stakeholderToUnstakes;
    mapping(uint256 => EpochInfo) public epochNumToEpochInfo;

    /* ========== CONSTRUCTOR ========== */

    constructor(
        address _stakingToken, 
        uint256 _beginDate,
        uint256 _defaultEpochRewardSize,
        uint256 _maxRateNumerator,
        uint256 _maxRateDenumenator,
        uint256 _numberOfEpochs,
        uint256 _epochDuration
    )
    Ownable()
    {
        stakingToken = IERC20(_stakingToken);
        BEGIN_DATE = _beginDate;
        DEFAULT_REWARD_SIZE = _defaultEpochRewardSize;
        MAX_RATE_NUM = _maxRateNumerator;
        MAX_RATE_DENUM = _maxRateDenumenator;
        NUMBER_OF_EPOCHS = _numberOfEpochs;
        EPOCH_DURATION = _epochDuration;
    }

    /* ========== VIEWS ========== */

    function getCurrentEpoch() public view returns(uint256) {
        uint256 epochNum = (block.timestamp - BEGIN_DATE) / EPOCH_DURATION;

        // Epoch 0 - only staking
        // Epoch (NUMBER_OF_EPOCHS + 1) - only unstaking
        return epochNum < NUMBER_OF_EPOCHS + 1 ? epochNum : NUMBER_OF_EPOCHS + 1;
    }

    function getNextEpoch() public view returns(uint256) {
        return getCurrentEpoch() + 1;
    }

    function getStakesByEpoch(uint256 epochNum, address userAddr)
        private 
        view 
        returns(EpochData memory) {

        EpochData memory prevEpoch = EpochData(0, 0, 0, 0, 0, 0, 0);

        uint256[] memory userStakes = stakeholderToStakes[userAddr];
        uint256[] memory userUnstakes = stakeholderToUnstakes[userAddr];

        for (uint256 epoch = 1; epoch <= epochNum; epoch++) {
            uint256 curTotalStake = prevEpoch.totalStake
                + prevEpoch.rewardSize
                + epochNumToEpochInfo[epoch].totalStakedAmount
                - epochNumToEpochInfo[epoch].totalUnstakedAmount;

            uint256 curUserStake = prevEpoch.userStake
                + prevEpoch.userReward
                + (userStakes.length > 0 ? userStakes[epoch] : 0) // + s_k^epoch
                - (userUnstakes.length > 0 ? userUnstakes[epoch] : 0); // - u_k^epoch


            // total
            uint256 curTotalMaxReward = (curTotalStake * MAX_RATE_NUM) / MAX_RATE_DENUM;
            uint256 curPoolMaxSize = DEFAULT_REWARD_SIZE + prevEpoch.rewardResidue;
            bool overrated = curTotalMaxReward < curPoolMaxSize;
            uint256 curPoolSize = overrated ? curTotalMaxReward : curPoolMaxSize;
            uint256 curPoolResidue = curPoolMaxSize - curPoolSize;

            // user
            uint256 curUserReward;
            if (overrated) {
                curUserReward = (curUserStake * MAX_RATE_NUM) / MAX_RATE_DENUM;
            } else {
                curUserReward = curTotalStake == 0 ? 0 : (curPoolSize * curUserStake) / curTotalStake;
            }

            prevEpoch.allTimeReward += prevEpoch.rewardSize;
            prevEpoch.allTimeStake += prevEpoch.totalStake;

            // total
            prevEpoch.rewardSize = curPoolSize;
            prevEpoch.rewardResidue = curPoolResidue;
            prevEpoch.totalStake = curTotalStake;

            // user
            prevEpoch.userStake = curUserStake;
            prevEpoch.userReward = curUserReward;
        }

        return prevEpoch;
    }

    function getRates() public view returns(uint256, uint256, uint256, uint256) {
        uint256 curEpoch = getCurrentEpoch();
        EpochData memory epoch = getStakesByEpoch(curEpoch, address(0));

        return (epoch.totalStake, epoch.rewardSize, epoch.allTimeReward, epoch.allTimeStake);
    }

    function getCurrentEpochStakes(address userAddr) public view returns(uint256, uint256, uint256) {
        uint256 curEpoch = getCurrentEpoch();
        EpochData memory epoch = getStakesByEpoch(curEpoch, userAddr);

        return (epoch.totalStake, epoch.userStake, epoch.userReward);
    }

    function getNextEpochStakes(address userAddr) public view returns(uint256, uint256, uint256, uint256) {
        uint256 nextEpoch = getNextEpoch();
        require(nextEpoch <= NUMBER_OF_EPOCHS, "Staking is closed");

        EpochData memory epoch =  getStakesByEpoch(nextEpoch, userAddr);

        uint256[] memory userStakes = stakeholderToStakes[userAddr];
        uint256 userNextEpochStake = userStakes.length > 0 ? userStakes[nextEpoch] : 0;

        return (epoch.totalStake, epoch.userStake, epoch.rewardSize, userNextEpochStake);
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    function stake(uint256 amount) public {
        require(amount > 0, "Cannot stake 0");

        uint256 nextEpoch = getNextEpoch();
        require(nextEpoch <= NUMBER_OF_EPOCHS, "Staking is closed");

        uint256 stakesLength = stakeholderToStakes[msg.sender].length;
        if (stakesLength == 0) {
            stakeholderToStakes[msg.sender] = new uint256[](NUMBER_OF_EPOCHS + 2);
        }

        stakeholderToStakes[msg.sender][nextEpoch] += amount;
        epochNumToEpochInfo[nextEpoch].totalStakedAmount += amount;

        stakingToken.safeTransferFrom(msg.sender, address(this), amount);

        emit Staked(msg.sender, amount);
    }

    function unstakeNext(uint256 amount) public {
        require(amount > 0, "Cannot stake 0");

        uint256 nextEpoch = getNextEpoch();
        require(nextEpoch <= NUMBER_OF_EPOCHS, "Staking is closed");

        uint256 stakesLength = stakeholderToStakes[msg.sender].length;
        require(stakesLength > 0, "Nothing staked");
        require(stakeholderToStakes[msg.sender][nextEpoch] >= amount, "Unstaking amount is too large");

        stakeholderToStakes[msg.sender][nextEpoch] -= amount;
        epochNumToEpochInfo[nextEpoch].totalStakedAmount -= amount;

        stakingToken.safeTransfer(msg.sender, amount);

        emit UnstakedNext(msg.sender, amount);
    }

    function unstake(uint256 amount) public {
        require(amount > 0, "Cannot unstake 0");

        uint256 curEpoch = getCurrentEpoch();
        uint256 stakesLength = stakeholderToStakes[msg.sender].length;

        require(stakesLength > 0, "Nothing staked");

        uint256 unstakesLength = stakeholderToUnstakes[msg.sender].length;
        if (unstakesLength == 0) {
            stakeholderToUnstakes[msg.sender] = new uint256[](NUMBER_OF_EPOCHS + 2);
        }

        (, uint256 userEpochStake,) = getCurrentEpochStakes(msg.sender);
        require(amount <= userEpochStake, "Unstaking amount is too large");

        stakeholderToUnstakes[msg.sender][curEpoch] += amount;
        epochNumToEpochInfo[curEpoch].totalUnstakedAmount += amount;

        stakingToken.safeTransfer(msg.sender, amount);

        emit Unstaked(msg.sender, amount);
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    function claimResidue() external onlyOwner {
        uint256 curEpoch = getCurrentEpoch();
        require(curEpoch == NUMBER_OF_EPOCHS + 1, "Staking is not over");

        EpochData memory epoch = getStakesByEpoch(curEpoch - 1, address(0));
        require(epoch.rewardResidue > 0, "Cannot claim 0");

        stakingToken.safeTransfer(msg.sender, epoch.rewardResidue);
    }

    /* ========== EVENTS ========== */

    event Staked(address indexed user, uint256 amount);
    event Unstaked(address indexed user, uint256 amount);
    event UnstakedNext(address indexed user, uint256 amount);
}