import { ethers } from "hardhat";
import { QTKX, QTKX__factory } from "../typechain";

async function main() {
  const QTKX = await ethers.getContractFactory("QTKX") as QTKX__factory;

  const initialSupply = ethers.utils.parseEther("120000000");
  const qtkx = await QTKX.deploy(initialSupply) as QTKX;

  await qtkx.deployed();

  // 0x6903E70ce7746eb40f7fdFD3f348041bE0324411 - BSC Testnet
  // 0x9890E90b5F8beBa7DafB2fC9160dF3A72855F823
  // 0x8d7f9344AECA34c6707E0C206c6E4104D60e203a - BSC Mainnet
  console.log("QTKX deployed to:", qtkx.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
