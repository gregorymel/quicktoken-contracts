import { ethers } from "hardhat";
import { StakingRewards, StakingRewards__factory, QTKX__factory } from "../typechain";

async function main() {
  const QTKX = await ethers.getContractFactory("QTKX") as QTKX__factory;
  const StakingRewards = await ethers.getContractFactory("StakingRewards") as StakingRewards__factory;

  const tokenAddress = "0x9890E90b5F8beBa7DafB2fC9160dF3A72855F823";
  const token = QTKX.attach(tokenAddress);

  // const hourInseconds = 60 * 60;
  const monthInSeconds = 60 * 60 * 24 * 30;
  const START_DATE = Math.floor(new Date().getTime() / 1000);
  const EPOCH_NUM = 12;
  const reward = "300000";
  const EPOCH_REWARD = ethers.utils.parseEther(reward);

  console.log("StartDate", START_DATE);

  const contract = await StakingRewards.deploy(
    tokenAddress, 
    START_DATE,
    EPOCH_REWARD,
    1,
    10,
    EPOCH_NUM,
    monthInSeconds
  ) as StakingRewards;

  await contract.deployed();
  console.log("StakingRewards deployed to:", contract.address);

  const transferTx = await token.transfer(contract.address, EPOCH_REWARD.mul(EPOCH_NUM));
  await transferTx.wait();

  // 0x47E8D788f1B136D704fc8663EcAE2Ef20C1C6CA9 - BSC Testnet
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
