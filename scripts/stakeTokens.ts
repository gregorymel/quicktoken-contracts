import { ethers } from "hardhat";
import { StakingRewards, StakingRewards__factory, QTKX__factory } from "../typechain";

async function main() {
  const QTKX = await ethers.getContractFactory("QTKX") as QTKX__factory;
  const StakingRewards = await ethers.getContractFactory("StakingRewards") as StakingRewards__factory;

  const tokenAddress = "0x6903E70ce7746eb40f7fdFD3f348041bE0324411";
  const token = QTKX.attach(tokenAddress);

  const contractAddress = "0x47E8D788f1B136D704fc8663EcAE2Ef20C1C6CA9";
  const contract = StakingRewards.attach(contractAddress);

//   const startTime = await contract.BEGIN_DATE();

//   console.log(startTime);

  const nextEpoch = await contract.getNextEpoch();

  const amount = ethers.utils.parseEther("888");

  const allowanceTx = await token.increaseAllowance(contract.address, amount);
  await allowanceTx.wait();

  const stakeTx = await contract.stake(amount, { gasLimit: 500000 });
  await stakeTx.wait();

  console.log(`Staked ${amount.toString()} in epoch ${nextEpoch.toNumber()}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
