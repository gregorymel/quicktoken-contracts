import { expect } from "chai";
import { BigNumber, BigNumberish } from "ethers";
import { ethers } from "hardhat";
import { StakingRewards, QTKX, QTKX__factory, StakingRewards__factory } from '../typechain';

describe("StakingReward", function () {
    let owner: any;
    let user1: any;
    let users: any[];
    let token: QTKX;
    let stakingRewards: StakingRewards;
    // let START_DATE: number;

    async function faucet(user: any, amount: BigNumberish) {
        const transferTx = await token.connect(owner).transfer(user.address, amount);
        await transferTx.wait();
    }

    async function allowance(user: any, amount: BigNumberish) {
        const allowanceTx = await token.connect(user).increaseAllowance(stakingRewards.address, amount);
        await allowanceTx.wait();
    }

    async function unstake(user: any, amount: BigNumberish) {
        const stakeTx = await stakingRewards.connect(user).unstake(amount);
        await stakeTx.wait();
    }

    async function unstakeTx(user: any, amount: BigNumberish) {
        const unstakeTx_ = await stakingRewards.connect(user).unstake(amount);
        return unstakeTx_;
    }

    async function stake(user: any, amount: BigNumberish) {
        await faucet(user, amount);
        await allowance(user, amount);
        const stakeTx = await stakingRewards.connect(user).stake(amount);
        await stakeTx.wait();
    }

    async function stakeTx(user: any, amount: BigNumberish) {
        await faucet(user, amount);
        await allowance(user, amount);
        const stakeTx_ = await stakingRewards.connect(user).stake(amount);
        return stakeTx_;
    }

    async function unstakeNext(user: any, amount: BigNumberish) {
        const unstakeTx = await stakingRewards.connect(user).unstakeNext(amount);
        await unstakeTx.wait();
    }

    async function nextEpoch(epochNum: number) {
        const duration = await stakingRewards.EPOCH_DURATION();
        const startDate = await stakingRewards.BEGIN_DATE();
        const newTimestampInSeconds = (duration.mul(epochNum)).add(startDate).toNumber();
        await ethers.provider.send("evm_mine", [newTimestampInSeconds]);
    }

    beforeEach(async () => {
        const accounts: any[] = await ethers.getSigners();
        owner = accounts[0];
        user1 = accounts[1];
        users = accounts.slice(2);
        const Token = (await ethers.getContractFactory("QTKX")) as QTKX__factory;
        const StakingRewards = await ethers.getContractFactory("StakingRewards") as StakingRewards__factory;

        const initialSupply = ethers.utils.parseEther("100000");
        token = (await Token.connect(owner).deploy(initialSupply)) as QTKX;
        await token.deployed();

        const latestBlock = await ethers.provider.getBlock("latest")
        const startDate = latestBlock.timestamp;

        stakingRewards = (await StakingRewards.connect(owner)
            .deploy(
                token.address, 
                startDate,
                ethers.utils.parseEther("100"),
                1,
                10,
                6,
                60 * 60 * 24 * 7
            )) as StakingRewards;
        
        
        await stakingRewards.deployed();

        const epochs = await stakingRewards.NUMBER_OF_EPOCHS();
        const defaultPool = await stakingRewards.DEFAULT_REWARD_SIZE();
        await faucet(stakingRewards, defaultPool.mul(epochs));
    });

    it("should stake tokens", async function () {
        // stake first time
        const NEXT_EPOCH_1 = 1;

        const amount = BigInt(10) ** BigInt(18);
        await stake(user1, amount);
        const realResult1 = await stakingRewards.stakeholderToStakes(user1.address, NEXT_EPOCH_1);
        expect(realResult1.eq(amount)).to.be.true;


        // stake second time in the same epoch
        await stake(user1, amount);
        const realResult2 = await stakingRewards.stakeholderToStakes(user1.address, NEXT_EPOCH_1);
        expect(realResult2.eq(amount + amount)).to.be.true;


        // stake first time in new epoch
        const NEXT_EPOCH_2 = 2;

        await nextEpoch(1);

        await stake(user1, amount);
        const realResult3 = await stakingRewards.stakeholderToStakes(user1.address, NEXT_EPOCH_2);
        expect(realResult3.eq(amount)).to.be.true;
    });

    it("getCurrentEpochStakes", async function () {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);
        
        const [totalStake, userStake, userReward] = await stakingRewards.getCurrentEpochStakes(users[0].address);
        expect(totalStake.eq(amount.mul(3))).to.be.true;
    });

    it("should return result with MAX epoch rate", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        const maxRateNumerator = await stakingRewards.MAX_RATE_NUM();
        const maxRateDenumerator = await stakingRewards.MAX_RATE_DENUM();

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(2);

        const [totalStake, userStake, userReward] = await stakingRewards.getCurrentEpochStakes(users[0].address);

        const totalStakeAtStart = amount.mul(3);

        const expectedTotalStake = (totalStakeAtStart.mul(maxRateNumerator).div(maxRateDenumerator))
            .add(totalStakeAtStart);
        const expectedUserStake = amount.mul(maxRateNumerator).div(maxRateDenumerator)
            .add(amount);

        expect(totalStake.eq(expectedTotalStake)).to.be.true;
        expect(userStake.eq(expectedUserStake)).to.be.true;
    });

    it("should return result with floating", async function() {
        const defaultEpochPool = await stakingRewards.DEFAULT_REWARD_SIZE();
        const amount = defaultEpochPool.mul(5);

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(2);

        const [totalStake, userStake, userReward] = await stakingRewards.getCurrentEpochStakes(users[0].address);

        const totalStakeAtStart =  amount.mul(3);

        const expectedTotalStake = (totalStakeAtStart.mul(defaultEpochPool).div(totalStakeAtStart))
            .add(totalStakeAtStart);
        const expectedUserStake = amount.mul(defaultEpochPool).div(totalStakeAtStart)
            .add(amount);

        expect(totalStake.eq(expectedTotalStake)).to.be.true;
        expect(userStake.eq(expectedUserStake)).to.be.true;
    });

    it("should carry pool residue to the next epoch", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(2);
        const [totalStake, userStake, userReward] = await stakingRewards.getCurrentEpochStakes(users[0].address);
        // todo: how to test it?
    });

    it("should unstake tokens", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);

        const unstakeAmount = amount.div(2);
        await unstake(users[0], unstakeAmount);

        const balance = await token.balanceOf(users[0].address);
        expect(balance.eq(unstakeAmount)).to.be.true;
    });

    it("should recalculate reward after partial unstaking", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);

        const unstakeAmount = amount.div(2);

        const [,, userRewardBefore] = await stakingRewards.getCurrentEpochStakes(users[0].address);
        await unstake(users[0], unstakeAmount);

        const [,, userRewardAfter] = await stakingRewards.getCurrentEpochStakes(users[0].address);
        expect(userRewardAfter.mul(2).eq(userRewardBefore)).to.be.true;

        await nextEpoch(2);
        const [,, userRewardEpoch2_0] = await stakingRewards.getCurrentEpochStakes(users[0].address);
        const [,, userRewardEpoch2_1] = await stakingRewards.getCurrentEpochStakes(users[1].address);

        expect(userRewardEpoch2_0.mul(2).eq(userRewardEpoch2_1)).to.be.true;
    });

    it("should recalculate reward after staking", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);

        const [, userStakeEpoch1, userRewardEpoch1] = await stakingRewards.getCurrentEpochStakes(users[0].address);

        await stake(users[0], amount);

        await nextEpoch(2);

        const [, userStakeEpoch2,] = await stakingRewards.getCurrentEpochStakes(users[0].address);

        const expectedUserStake = userStakeEpoch1.add(userRewardEpoch1).add(amount);

        expect(expectedUserStake.eq(userStakeEpoch2)).to.be.true;
    });

    it("should revert tx on unstake is too large", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);

        const tx = unstakeTx(users[0], amount.add(1));
        expect(tx).to.be.reverted;
    });

    it("shouldn't stake when time is up", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);

        const numOfEpochs = await stakingRewards.NUMBER_OF_EPOCHS();
        await nextEpoch(numOfEpochs.toNumber());

        const tx = stakeTx(users[0], amount);
        expect(tx).to.be.reverted;
    });

    it("should unstake when time is up", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);

        const numOfEpochs = await stakingRewards.NUMBER_OF_EPOCHS();
        await nextEpoch(numOfEpochs.toNumber() + 5);

        await unstake(users[0], amount);
        const epochNum = (await stakingRewards.getCurrentEpoch()).toNumber();

        expect(epochNum).to.equal(numOfEpochs.toNumber() + 1);
    });

    it("should return next epoch stakes", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(1);

        await stake(users[0], amount);
        const [, , , nextStake] = await stakingRewards.getNextEpochStakes(users[0].address);

        expect(nextStake.eq(amount)).to.be.true;
    });

    it.skip("should return historical rates", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await stake(users[1], amount);
        await stake(users[2], amount);

        await nextEpoch(2);

        const rates = await stakingRewards.getRates();
        console.log(rates);
    });

    it("should claim non-zero residue", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);

        const numOfEpochs = await stakingRewards.NUMBER_OF_EPOCHS();
        await nextEpoch(numOfEpochs.toNumber() + 5);

        const balanceBefore = await token.balanceOf(stakingRewards.address);
        console.log(balanceBefore);

        const tx = await stakingRewards.claimResidue();
        await tx.wait();

        await unstake(users[0], amount);

        const balanceAfter = await token.balanceOf(stakingRewards.address);
        // expect
        console.log(balanceAfter);
    });

    it("should unstake from the next epoch", async function() {
        const amount: BigNumber = ethers.utils.parseEther("1");

        await stake(users[0], amount);
        await unstakeNext(users[0], amount);

        const [,,,nextStake] = await stakingRewards.getNextEpochStakes(users[0].address);
        expect(nextStake.toNumber()).to.equal(0);
    });

});