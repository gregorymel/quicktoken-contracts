const ethers = require("ethers");

const tokenAddress = "0x9890E90b5F8beBa7DafB2fC9160dF3A72855F823";
// const hourInseconds = 60 * 60;
const monthInSeconds = 60 * 60 * 24 * 30;
const START_DATE = 1649331529;
const EPOCH_REWARD = ethers.utils.parseEther("300000").toString();
const EPOCH_NUM = 12;

module.exports = [
    tokenAddress, 
    START_DATE,
    EPOCH_REWARD,
    1,
    10,
    EPOCH_NUM,
    monthInSeconds
];