import { BigNumber, ethers } from "ethers";
import { StakingRewards } from "../typechain";
import { StakingRewardsInterface } from "./abi";
import * as dotenv from "dotenv";

dotenv.config();

const TOKEN_ADDR = "0x6903E70ce7746eb40f7fdFD3f348041bE0324411";
const STAKING_ADDR = "0x47E8D788f1B136D704fc8663EcAE2Ef20C1C6CA9";

function estimateReward(inputValue: number, totalStakeNext: BigNumber, totalRewardNext: BigNumber) {
    const rate = 1 / totalStakeNext.div(totalRewardNext).toNumber();
    const inputValueBN = ethers.utils.parseEther(inputValue.toFixed(5));
    const newRate = 1 / inputValueBN.add(totalStakeNext).div(inputValueBN).toNumber();
    const realRate = newRate < rate ? newRate : rate;

    return inputValue * realRate;
}

async function main() {
    // const provider = new ethers.providers.Web3Provider(window.ethereum)
    const provider = new ethers.providers.JsonRpcProvider(process.env.BSC_TESTNET_URL);

    if (!process.env.BSC_TEST_PRIVATE_KEY) return;

    const signer = new ethers.Wallet(process.env.BSC_TEST_PRIVATE_KEY, provider);

    const Staking = new ethers.Contract(STAKING_ADDR, StakingRewardsInterface, signer) as StakingRewards;

    //############## Stake balance - Button
    const amountStake = ethers.utils.parseEther("2");
    const stakeTx = await Staking.stake(amountStake, { gasLimit: 500000 });
    await stakeTx.wait();

    console.log(`Staked ${amountStake}`);

    //############## Unstake from the next epoch - Button
    const amountUnstakeNext = ethers.utils.parseEther("1");
    const unstakeNextTx = await Staking.unstakeNext(amountUnstakeNext, { gasLimit: 500000 });
    await unstakeNextTx.wait();

    console.log(`Staked ${amountUnstakeNext}`);

    //############## Unstake - Button
    const amountUnstake = ethers.utils.parseEther("1");
    const unstakeTx = await Staking.unstake(amountUnstake, { gasLimit: 500000 });
    await unstakeTx.wait();

    console.log(`Staked ${amountUnstake}`);

    //############## Next epoch - Data
    const [totalStakeNext, userStakeNext, totralRewardNext, stakeNext] = await Staking.getNextEpochStakes(signer.address);
    const reward = estimateReward(100, totalStakeNext, totralRewardNext);

    console.log(reward);

    // ############## Current epoch - Data
    const [totalStake, userStake, userReward] = await Staking.getCurrentEpochStakes(signer.address);

    // ############## Historcal Data
    const [totalStake_, rewardSize, allTimeReward, allTimeStake] = await Staking.getRates();
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});