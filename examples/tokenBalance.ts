import { ethers } from "ethers";
import { QTKX } from "../typechain";
import { QTKXInterface } from "./abi";
import * as dotenv from "dotenv";

dotenv.config();

const TOKEN_ADDR = "0x6903E70ce7746eb40f7fdFD3f348041bE0324411";
const STAKING_ADDR = "0xD6d110E00007a998D5f28f37bB2cfb16Be79b2AA";

async function main() {
    // const provider = new ethers.providers.Web3Provider(window.ethereum)
    const provider = new ethers.providers.JsonRpcProvider(process.env.BSC_TESTNET_URL);

    if (!process.env.BSC_TEST_PRIVATE_KEY) return;

    const signer = new ethers.Wallet(process.env.BSC_TEST_PRIVATE_KEY, provider);

    // Token balance
    const QTKX = new ethers.Contract(TOKEN_ADDR, QTKXInterface, signer) as QTKX;
    const balance = await QTKX.balanceOf(signer.address);

    console.log(`Balance of ${signer.address} = ${balance}`);

    // Increase allowance
    const value = ethers.utils.parseEther("1000");
    const allowanceTx = await QTKX.increaseAllowance(STAKING_ADDR, value);
    await allowanceTx.wait();

    console.log("Allowance increased");
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});